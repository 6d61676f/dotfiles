#zmodload zsh/zprof

autoload -Uz compinit promptinit

# create a zkbd compatible hash;
# to add other keys to this hash, see: man 5 terminfo
typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

# setup key accordingly
bindkey -e

[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"      beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"       end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"    overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}" backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"    delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"        history-beginning-search-backward
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"      history-beginning-search-forward
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"      backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"     forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"    beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"  end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}" reverse-menu-complete

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
	function zle_application_mode_stop { echoti rmkx }
	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

setopt share_history			# This option both imports new commands from the history file.
setopt hist_save_no_dups		# older commands that duplicate newer ones are omitted.
setopt hist_reduce_blanks		# Remove superfluous blanks from each command line.
setopt hist_ignore_dups			# ignore duplicated commands history list
setopt hist_expire_dups_first	# delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_verify				# show command with history expansion to user before running it

setopt correct                  # try to correct commands
setopt correct_all				# try to correct everything
setopt auto_list				# automatically list choices on ambiguous completion.
setopt auto_menu				# automatically use menu completion.
unsetopt menu_complete			# do not autoselect the first completion entry
setopt always_to_end			# move cursor to end if word had one match.
setopt complete_aliases			# Prevents aliases on the command line from being
								# internally substituted before completion is attempted.
setopt complete_in_word			# try to complete from within word.
setopt list_ambiguous			# complete until longest common substring

unsetopt flow_control			# disable start(^S)/stop(^Q) in shell editor.
setopt interactive_comments		# can use # in cli
setopt no_clobber				# truncate using >!
setopt print_exit_value			# when != EXIT_SUCCESS


HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=$HISTSIZE

#WORDCHARS="${WORDCHARS/\/}"
#WORDCHARS="${WORDCHARS/./}"
#WORDCHARS="${WORDCHARS/-/}"
#WORDCHARS="${WORDCHARS/_/}"
WORDCHARS=""

zstyle ':completion:*' menu yes select							# show menu for completion
zstyle ':completion:*' special-dirs true						# ./ ../
zstyle ':completion:*' rehash true								# find new binaries in PATH
zstyle ':completion:*:ssh:*' hosts off							# Disable SSH hosts completion

[[ -z $(command -v dircolors) ]] \
    || eval $(dircolors -b ${XDG_CONFIG_HOME}/dircolors/config)	# generate LS_COLORS

zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}			# colors !

zstyle ':completion:*' completer _expand_alias _expand\
	   _complete _correct _approximate							# list of completers to use
zstyle ':completion:*' use-cache yes							# the completion caching layer
																# is activated for any completions which use it
zstyle ':completion:*' cache-path $ZSH_CACHE_DIR				# path to completion cache

[ -f ~/.aliases ] && source ~/.aliases

#Please place sheldon and fzf in path
ZSH_AUTOSUGGEST_USE_ASYNC=1
source <(sheldon source)

compinit -i

promptinit
prompt adam2

#REPORTTIME=1
#zprof

echo "$("${FORTUNE:-:}" -so)"
