# XDG Related
[ -z "$XDG_CONFIG_HOME" ] && export XDG_CONFIG_HOME="$HOME/.config"
[ -z "$XDG_CACHE_HOME" ]  && export XDG_CACHE_HOME="$HOME/.cache"
[ -z "$XDG_DATA_HOME" ]   && export XDG_DATA_HOME="$HOME/.local/share"

# Rust Env
[ -z "$CARGO_HOME" ]     && export CARGO_HOME="$XDG_DATA_HOME/cargo"
[ -d "$CARGO_HOME/bin" ] && export PATH="${PATH}:${CARGO_HOME}/bin"
[ -z "$RUSTUP_HOME" ]    && export RUSTUP_HOME="$XDG_DATA_HOME/rustup"

# Golang Env
[ -z "$GOPATH" ]       && export GOPATH="$XDG_DATA_HOME/go"
[ -d "${GOPATH}/bin" ] && export PATH="${PATH}:${GOPATH}/bin"

# Java Env
export _JAVA_OPTIONS="$_JAVA_OPTIONS
-Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel
-Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel
-Dawt.useSystemAAFontSettings=on
-Dswing.aatext=true
-Djdk.gtk.version=3"
export _JAVA_AWT_WM_NONREPARENTING=1

# Editor/Pager
# Pager
for i in bat most less more; do
    PAGER="$(command -v $i)"
    if [ -n "$PAGER" ]; then
        export PAGER
        break
    fi
done
# Editor
for i in nvim vim vi emacs nano; do
    EDITOR="$(command -v $i)"
    if [ -n "$EDITOR" ]; then
        VISUAL="$EDITOR"
        SUDO_EDITOR="$EDITOR"
        export EDITOR VISUAL SUDO_EDITOR
        break
    fi
done

# Misc
[ -x "$(command -v misfortune)" ] && export FORTUNE='misfortune'
[ -z "$FREETYPE_PROPERTIES" ]  && export FREETYPE_PROPERTIES='truetype:interpreter-version=40'

# Additional path folders
[ -d "${HOME}/.local/bin" ] && export PATH="${HOME}/.local/bin:${PATH}"
