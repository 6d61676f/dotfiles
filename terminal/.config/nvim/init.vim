set nocompatible               " be iMproved, required
set number                     " show line number
filetype off                   " required
filetype plugin on
syntax on
set t_Co=256                   " Terminal 256 colors
set encoding=utf-8
set shortmess+=I               " No intro message
set hidden                     " Allow buffer switching without saving
set smarttab
set expandtab                  " Tabs are spaces, not tabs
set shiftwidth=4               " Use indents of 4 spaces
set tabstop=4                  " An indentation every four columns
set softtabstop=4              " Let backspace delete indent
set cursorline                 " Highlight line with cursor
set backspace=indent,eol,start " Backspace for dummies
set linespace=0                " No extra spaces between rows
set showmatch                  " Show matching brackets/parenthesis
set incsearch                  " Find as you type search
set hlsearch                   " Highlight search terms
set ignorecase                 " Case insensitive search
set smartcase                  " Case sensitive when uc present
set wildmenu                   " Show list instead of just completing
"set wildmode=list:longest,full " Command <Tab> completion, list matches, then longest common part, then all.
set nojoinspaces               " Prevents inserting two spaces after punctuation on a join (J)
set splitright                 " Puts new vsplit windows to the right of the current
set splitbelow                 " Puts new split windows to the bottom of the current
set clipboard=unnamed          " Access your system clipboard with this line

" Bootstrap Plug
let autoload_plug_path = stdpath('data') . '/site/autoload/plug.vim'
if !filereadable(autoload_plug_path)
  silent execute '!curl -fLo ' . autoload_plug_path . '  --create-dirs
      \ "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
unlet autoload_plug_path

call plug#begin(stdpath('data') . '/plugged')

Plug 'itchyny/lightline.vim'
Plug 'ap/vim-buftabline'

Plug 'w0rp/ale'
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/nerdcommenter'

Plug 'skielbasa/vim-material-monokai'

Plug 'sbdchd/neoformat'

Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

Plug 'bronson/vim-trailing-whitespace'
Plug 'majutsushi/tagbar'
Plug 'Yggdroot/indentLine'
Plug 'mbbill/undotree'
Plug 'kshenoy/vim-signature'
Plug 'junegunn/vim-easy-align'

" Distraction-free dev
Plug 'junegunn/limelight.vim'
Plug 'junegunn/goyo.vim'

call plug#end()

"" Map leader to ,
let mapleader=','

"" Colorscheme
"let g:molokai_original = 1
"let g:rehash256 = 1
"color molokai
set background=dark
"set termguicolors "No color on tmux and some TERMS
colorscheme material-monokai

"Lightline
set noshowmode
set laststatus=2
function! ShowTag()
    return tagbar#currenttag('[%s]', '', 's')
endfunction
let g:lightline =  {
            \   'colorscheme':'solarized',
            \   'active': {
            \       'left': [ [ 'mode', 'paste' ],
            \                 [ 'gitbranch', 'readonly', 'modified' ],
            \                 [ 'current_tag' ] ] ,
            \       'right': [ [ 'percent' ] , [ 'lineinfo' ] ]
            \   },
            \ 'component_function': {
            \   'gitbranch': 'fugitive#head',
            \   'current_tag': 'ShowTag'
            \ },
            \ }

""ALE
let g:ale_sign_error         = '>>'
let g:ale_sign_warning       = '--'
let g:ale_sign_column_always = 1
let g:ale_lint_on_enter      = 1
"let g:ale_open_list          = 1
let g:ale_fixers = {
            \    'cpp': [
            \        'clang-format',
            \        'uncrustify',
            \    ],
            \    'c': [
            \        'clang-format',
            \        'uncrustify',
            \    ],
            \}

"Goyo
let g:goyo_width = '90%'
let g:goyo_height = '90%'

"Undo Tree
nnoremap <F5> :UndotreeToggle<cr>

" Tagbar
nmap <silent> <F4> :TagbarToggle<CR>
let g:tagbar_autofocus = 1

" NerdTree
map <C-n> :NERDTreeToggle<CR>

" Open Buffer
noremap <leader>bh :new<CR>
noremap <leader>bv :vnew<CR>

"" Buffer nav
noremap <leader>k :bn<CR>
noremap <leader>j :bp<CR>

"" Split
noremap <Leader>- :<C-u>split<CR>
noremap <Leader>\ :<C-u>vsplit<CR>

"" Close buffer
noremap <leader>d :bd<CR>

"" Switching windows
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
noremap <C-h> <C-w>h

" Wrapped lines goes down/up to next row, rather than next line in file.
noremap j gj
noremap k gk

" No More arrows
noremap <Up> <nop>
noremap <Down> <nop>
noremap <Left> <nop>
noremap <Right> <nop>


"Folds
"zi 	switch folding on or off
"zo 	open current fold
"zO 	recursively open current fold
"zc 	close current fold
"zC 	recursively close current fold
"za 	toggle current fold
"zA 	recursively open/close current fold
"zm 	reduce `foldlevel` by one
"zM 	close all folds
"zr 	increase `foldlevel` by one
"zR 	open all folds
"zj 	move down to top of next fold
"zk 	move up to bottom of previous fold

""Slows down vim very much
""Fold code by syntax
"set foldenable
"set foldmethod=syntax
"set foldlevelstart=2
"nnoremap <Space> za


"Marks
"  mx           Toggle mark 'x' and display it in the leftmost column
"  dmx          Remove mark 'x' where x is a-zA-Z
"
"  m,           Place the next available mark
"  m.           If no mark on line, place the next available mark. Otherwise, remove (first) existing mark.
"  m-           Delete all marks from the current line
"  m<Space>     Delete all marks from the current buffer
"  ]`           Jump to next mark
"  [`           Jump to prev mark
"  ]'           Jump to start of next line containing a mark
"  ['           Jump to start of prev line containing a mark
"  `]           Jump by alphabetical order to next mark
"  `[           Jump by alphabetical order to prev mark
"  ']           Jump by alphabetical order to start of next line having a mark
"  '[           Jump by alphabetical order to start of prev line having a mark
"  m/           Open location list and display marks from current buffer
"
"  m[0-9]       Toggle the corresponding marker !@#$%^&*()
"  m<S-[0-9]>   Remove all markers of the same type
"  ]-           Jump to next line having a marker of the same type
"  [-           Jump to prev line having a marker of the same type
"  ]=           Jump to next line having a marker of any type
"  [=           Jump to prev line having a marker of any type
"  m?           Open location list and display markers from current buffer
"  m<BS>        Remove all markers

