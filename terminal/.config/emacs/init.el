;;; init.el --- Emacs Configuration File

;;; Commentary:
;; Testing

;;; Code:

;;; Generic Config

;; Disable all bars
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(unless (eq window-system 'ns)
  (menu-bar-mode -1))
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode 0))
(when (fboundp 'horizontal-scroll-bar-mode)
  (horizontal-scroll-bar-mode -1))
;;(when (fboundp 'tooltip-mode)
;;  (tooltip-mode -1))
(when (fboundp 'fringe-mode)
  (fringe-mode -1))

(setq inhibit-startup-screen t)


(show-paren-mode 1)
(global-auto-revert-mode 1)
(global-display-line-numbers-mode 1)
(save-place-mode 1)
(global-set-key (kbd "M-z") 'zap-up-to-char)


;; Begone tabs
(setq-default tab-width 4 indent-tabs-mode nil)

;; Otherwise the default bar appears a bit before the custom one
(setq-default mode-line-format nil)
;; Get rid of assignment to free var
(defvar apropos-do-all)
(setq save-interprogram-paste-before-kill t apropos-do-all t mouse-yank-at-point t
      require-final-newline t load-prefer-newer t select-enable-primary t select-enable-clipboard t)
(setq backup-directory-alist `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))
;; Get rid of assignment to free var
(defvar user-config-dir)
(setq user-config-dir (expand-file-name "user" user-emacs-directory))
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
;; don't load custom-file. if we need anything just copy it here!
;; (when (file-exists-p custom-file)
;;   (load custom-file))
(add-to-list 'load-path user-config-dir)

;;; subword mode
(global-subword-mode t)


;;; Reload config
(defun my-config-reload ()
  "Reload init.el"
  (interactive)
  (load-file user-init-file))
(global-set-key (kbd "C-c r") 'my-config-reload)
;; Reload config


;;; Kill current buffer
(defun my-kill-current-buffer ()
  "Kill current buffer"
  (interactive)
  (kill-current-buffer))
(global-set-key (kbd "C-c k") 'my-kill-current-buffer)
;; Kill current buffer


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :extend nil :stipple nil :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight regular :height 113 :width normal :foundry "UKWN" :family "Iosevka Term"))))
 '(tab-bar ((t (:inherit fixed-pitch))))
 '(tab-bar-tab ((t (:inherit tab-bar-tab-inactive :inverse-video t))))
 '(tab-bar-tab-inactive ((t (:inherit tab-bar))))
 )


;; Enable y/n answers
(fset 'yes-or-no-p 'y-or-n-p)

;; Generic Config


;;; Bootstrap use-package
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)


(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  ;; Following line is not needed if use-package.el is in ~/.emacs.d
  ;; (add-to-list 'load-path "<path where use-package is installed>")
  (require 'use-package))
;; Bootstrap use-package


;;; Async
(use-package
  async
  :ensure t
  :init(dired-async-mode 1)
  :config(async-bytecomp-package-mode 1))
;; Async



;;;; tomorrow
;(use-package color-theme-sanityinc-tomorrow
; :ensure t
; :config (load-theme 'sanityinc-tomorrow-eighties t))
;;; tomorrow



(use-package nimbus-theme
  :ensure t
  :config
  (load-theme 'nimbus t))



;;;;; Monokai
;;(use-package
;;  monokai-theme
;;  :ensure t
;;  :config (load-theme 'monokai t))
;;;; Monokai



;;; Telephone line
(use-package
  telephone-line
  :ensure t
  :config (telephone-line-mode 1))
;;; Telephone line



;;; Expand region
(use-package
  expand-region
  :ensure t
  :bind ("C-c e" . er/expand-region))
;; Expand region


;;; Beacon
(use-package
  beacon
  :ensure t
  :config (beacon-mode 1))
;; Beacon


;;; Cmake font lock
(use-package
  cmake-font-lock
  :ensure t)
;; Cmake font lock


;;; Diff hl
(use-package
  diff-hl
  :ensure t
  :config (global-diff-hl-mode 1)
  (unless (display-graphic-p)
    (diff-hl-margin-mode 1)))
;; Diff hl


;;; Magit
(use-package
  magit
  :ensure t
  :bind ("C-c g" . magit-status))
;; Magit


;;; Goto line preview
(use-package
  goto-line-preview
  :ensure t
  :bind ([remap goto-line] . goto-line-preview))
;;; Goto line preview


;;; Rainbow delimiteers
(use-package
  rainbow-delimiters
  :ensure t
  :hook (prog-mode . rainbow-delimiters-mode))
;; Rainbow delimiteers


;;; Undo-Tree
(use-package
  undo-tree
  :ensure t
  :config (global-undo-tree-mode)
  (setq undo-tree-history-directory-alist `(("." . ,(concat user-emacs-directory "undo-tree")))))
;; Undo-Tree


;;; Smooth Scrolling
(setq scroll-margin 0 scroll-step 1 scroll-conservatively 10000 scroll-preserve-screen-position 1)
;; Smooth Scrolling


;;; Xclip
(use-package
  xclip
  :ensure t
  :config(xclip-mode))
;; Xclip


;;; Company
(use-package
  company
  :ensure t
  ;; XXX: For some reason this doesn't work in terminal mode
  ;; Note tab <tab> and TAB are different keys. TAB works in terminal
  :bind("TAB" . company-indent-or-complete-common)
  :bind((:map company-active-map
              ("C-n" . company-select-next)
              ("C-p" . company-select-previous)
              ))
  :config (setq company-idle-delay 0)
  (setq company-minimum-prefix-length 2)
  (global-company-mode))
;;; Company


;;; Flycheck
(use-package
  flycheck
  :ensure t
  :hook (prog-mode . flycheck-mode))
;; Flycheck


;;; Lsp
(use-package
  lsp-mode
  :ensure t
  :bind-keymap ("C-c l" . lsp-command-map)
  :hook ((lsp-mode . lsp-enable-which-key-integration)
         ;; C/C++
         (c-mode . lsp)
         (c++-mode . lsp))
  :config(setq lsp-keymap-prefix "C-c l")
  :commands lsp)
;; Lsp


;;; Lsp ui
(use-package
  lsp-ui
  :ensure t
  :commands lsp-ui-mode
  :config (setq lsp-ui-doc-position 'bottom)
  )
;; Lsp ui


;;; Projectile
(use-package
  projectile
  :ensure t
  :bind-keymap ("C-c p" . projectile-command-map)
  :config (projectile-mode 1))
;; Projectile


;;; YaSnippet snips
(use-package yasnippet
  :ensure t
  :config
  (use-package yasnippet-snippets
    :ensure t)
  :hook(prog-mode . yas-minor-mode))
;; YaSnippet snips


;;; Rustic
(use-package
  rustic
  :ensure t)
;; Rustic


;;; Orderless
(use-package orderless
  :ensure t
  :config
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles basic partial-completion)))))
;; Orderless



;;; Vertico
(use-package vertico
  :ensure t
  :config
  (vertico-mode)
  (setq vertico-cycle t)
)
;; Vertico



;;; save-hist
(use-package savehist
  :ensure t
  :config
  (savehist-mode))
;; save-hist



;;; Consult
(use-package consult
  :ensure t
  :bind(("M-y" . consult-yank-pop)
        ("C-x b" . consult-buffer)
        ("C-x 4 b" . consult-buffer-other-window)
        ("M-g g" . consult-goto-line)
        ("C-c i" . consult-imenu)
        ("M-s r" . consult-ripgrep)
        ("M-s l" . consult-locate)
        ;; Isearch integration
        ("M-s e" . consult-isearch-history)
        :map isearch-mode-map
        ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
        ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
        ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
        ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
        )
  :init)
;; Consult



;; Enable richer annotations using the Marginalia package
(use-package marginalia
  ;; Either bind `marginalia-cycle` globally or only in the minibuffer
  :ensure t
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))

  ;; The :init configuration is always executed (Not lazy!)
  :init

  ;; Must be in the :init section of use-package such that the mode gets
  ;; enabled right away. Note that this forces loading the package.
  (marginalia-mode)
 )



;;;; Swiper
;(use-package
;  swiper
;  :ensure t
;  :bind(("C-s" . swiper-isearch-thing-at-point)
;        ("C-r" . swiper-isearch-thing-at-point)
;        ))
;;; Swiper



;;; WhichKey
(use-package
  which-key
  :ensure t
  :config (which-key-setup-side-window-right-bottom)
  ;; Show key sequence possibilities
  (which-key-mode 1))
;; WhichKey


;;; Switch Window
(use-package
  switch-window
  :ensure t
  :bind("C-x o" . switch-window)
  :config(setq switch-window-shortcut-appearance 'asciiart)
  ;;(setq switch-window-auto-resize-window t)
  )
;; Switch Window


;;; crux
(use-package
  crux
  :ensure t
  :bind(("C-k" . crux-smart-kill-line)
        ("C-c n" . crux-cleanup-buffer-or-region)
        ("C-c t" . crux-visit-term-buffer)
        ("M-j" . crux-join-line)
        ("C-c I" . crux-find-user-init-file)
        ("M-o" . crux-other-window-or-switch-buffer)
        ("C-<backspace>" . crux-kill-line-backwards)
        ([remap move-beginning-of-line] . crux-move-beginning-of-line)
        ))
;; crux

;;; format-all
(use-package
  format-all
  :ensure t)
;; format-all


;;; Window resize
(global-set-key (kbd "M-s <left>") 'shrink-window-horizontally)
(global-set-key (kbd "M-s <right>") 'enlarge-window-horizontally)
(global-set-key (kbd "M-s <down>") 'shrink-window)
(global-set-key (kbd "M-s <up>") 'enlarge-window)
(global-set-key (kbd "M-s =") 'balance-windows)
(global-set-key (kbd "M-s m") 'maximize-window)
;; Window resize


;;; Tab-bar
(setq tab-bar-close-last-tab-choice 'tab-bar-mode-disable)
(setq tab-bar-show 1)
;;(setq tab-bar-format nil)
(setq tab-bar-close-button-show nil)
(global-set-key (kbd "C-x t n") 'tab-bar-switch-to-next-tab)
(global-set-key (kbd "C-x t p") 'tab-bar-switch-to-prev-tab)
;; Tab-bar


;;; init.el ends here
