#!/usr/bin/env bash

source bspwm_utils.bash || exit 1

if echo_pick 'Switch To:'; then
    bspc node "$__BSPWM_NID" -s biggest.local
fi
