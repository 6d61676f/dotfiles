#!/usr/bin/env bash

source bspwm_utils.bash || exit 1
echo_pick 'Monocolize:'

if echo_pick 'Monocolize:'; then
    bspc node "$__BSPWM_NID" --focus
    bspc desktop --layout monocle
fi

