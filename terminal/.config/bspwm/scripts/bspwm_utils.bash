#!/usr/bin/env bash

declare -A nodes
names=""
for i in $(bspc query --nodes --desktop); do
    name="$(xdotool getwindowname "$i" 2>/dev/null)" \
	&& nodes["$name"]="$i" \
	&& names="$name\n$names"
done
names=${names%\\n}

function echo_pick
{
    __BSPWM_PICK="$(echo -e "$names" | dmenu -f -p "${1:-Run}")"

    if [ -z "$__BSPWM_PICK" ]; then 
	return 1
    else
	__BSPWM_NID="${nodes["$__BSPWM_PICK"]}"
	return 0
    fi
}

