#!/usr/bin/env bash

if [ -f "$BSPWM_SIDFILE" ]; then
    rm "$BSPWM_SIDFILE"
fi


command -v dmenu || bspc quit

if [ "$(printf '%s\n%s' No Yes | dmenu -p 'Exit WM?' -f )" = "Yes" ]; then
    bspc quit
fi
