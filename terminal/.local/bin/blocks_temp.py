#!/usr/bin/env python3
from subprocess import run, PIPE
import re
import sys


def get_temp(device_name):
    """
    Functia ia ca argument numele unui device din output-ul normal de 'sensors'
    Ruleaza 'sensors' si cauta linia respectiva cu regex,
    identificand toate valorile de forma 'numar.numar' care reprezinta
    (valoare curenta, valoare ridicata, valoare maxima)
    In cazul in care esueaza, returneaza 'None',
    altfel va returna o lista de valori
    """
    try:
        execution = run(["sensors"], stdout=PIPE)
        if execution.returncode == 0:
            output = execution.stdout.decode("utf-8")
            lineregex = re.compile(
                "^{}.*$".format(device_name), re.MULTILINE | re.IGNORECASE)
            tempregex = re.compile("(\d+?.\d+?)")
            line = re.search(lineregex, output)
            result = re.findall(tempregex, line.group(0))
            result = list(map(float, result))
            return result
        else:
            return None
    except Exception as inst:
        return None


def get_temp2(get_max):
    try:
        if get_max == True:
            file_s = '/sys/class/hwmon/hwmon1/temp1_crit'
        else:
            file_s = '/sys/class/hwmon/hwmon1/temp1_input'
        with open(file_s) as f:
            temp = f.read()
            temp = float(temp)
            if temp > 1000:
                temp = temp/1000
            return temp
    except Exception as inst:
        return None


def pangoPrintTemp(arrayTemp):
    """
    Primeste o lista de temperaturi de forma [curent,ridicat,max] sau
    [curent,max] sau [curent] si va printa cu format pango daca valoarea
    este admisibila
    """
    red = "#ff0000"
    green = "#00ff00"
    yellow = "#ffff00"

    if arrayTemp is None:
        print("arrayTemp is None")
        return

    if len(arrayTemp) == 3:
        if arrayTemp[0] < arrayTemp[1]:
            color = green
        elif arrayTemp[0] >= arrayTemp[1] and arrayTemp[0] < arrayTemp[2]:
            color = yellow
        else:
            color = red
    elif len(arrayTemp) == 2:
        if arrayTemp[0] < 0.8 * arrayTemp[1]:
            color = green
        else:
            color = red
    elif len(arrayTemp) == 1:
        color = yellow
    else:
        color = red
        arrayTemp = [666]

    print('<span foreground="{0}">{1}°C</span>'.format(color, arrayTemp[0]))
    return


if __name__ == '__main__':
    """
    if len(sys.argv) < 2:
        print("Give device as argument")
    else:
        array = get_temp(sys.argv[1])
        pangoPrintTemp(array)
    """
    array = [get_temp2(False), get_temp2(True)]
    pangoPrintTemp(array)
