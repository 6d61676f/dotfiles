#!/usr/bin/env python3
import requests
from os.path import expanduser


def get_coord_weather(apiKey, coords):
    """
    Primeste ca argumente cheia api de OpenWeatherMap,
    coordonatele sub forma [latitudine, longitudine] si
    returneaza vremea curenta in format json
    Daca ceva esueaza, va returna 'None'
    """
    api = ("http://api.openweathermap.org"
           "/data/2.5/weather?lat={0}&lon={1}&appid={2}&units=metric")
    if apiKey is None or len(coords) != 2:
        return None
    api = api.format(coords[0], coords[1], apiKey)
    try:
        req = requests.get(api)
    except Exception as inst:
        return None
    if not req.ok:
        print(req.content)
        return None
    if req.json is None:
        return None
    else:
        return req.json()


def pangoPrintWeather(weather_json):
    if weather_json is None:
        print("Problem with weather app")
        return
    temp = weather_json['main']['temp']
    hum = weather_json['main']['humidity']
    press = weather_json['main']['pressure']
    name = weather_json['name']
    wind = weather_json['wind']['speed']
    clouds = weather_json['clouds']['all']
    print("{0}: {1}℃ {2}% {3}hPa {4}m/s {5}☁".format(name,
                                                     temp,
                                                     hum,
                                                     press,
                                                     wind,
                                                     clouds))


if __name__ == '__main__':
    try:
        with open("{}/.owm_api".format(expanduser("~"))) as api:
            appid = api.readline().strip()
        coordsBuc = [44.42, 26.14]
        pangoPrintWeather(get_coord_weather(appid, coordsBuc))
    except Exception:
        print("Api file not found")
