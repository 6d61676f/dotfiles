#!/usr/bin/env python3
import multiprocessing
red = "#ff0000"
green = "#00ff00"
yellow = "#ffff00"


def get_load():
    try:
        with open('/proc/loadavg') as load_f:
            line = load_f.read().strip().split()
            rez = [line[0], line[1], line[2]]
            return [float(x) for x in rez]
    except Exception as inst:
        """
        print(inst)
        """
        return None


def get_state(load):
    if load < 0.5:
        return green
    elif load >= 0.5 and load < 0.75:
        return yellow
    else:
        return red


def pango_print(load_arr):
    if len(load_arr) != 3:
        return None
    else:
        one = get_state(load_arr[0])
        five = get_state(load_arr[1])
        fifteen = get_state(load_arr[2])
    output = '<span foreground="{0}">{1:.2f}</span> '.format(one, load_arr[0])
    output += '<span foreground="{0}">{1:.2f}</span> '.format(
        five, load_arr[1])
    output += '<span foreground="{0}">{1:.2f}</span>'.format(
        fifteen, load_arr[2])
    print(output)
    return


if __name__ == '__main__':
    cpu_count = multiprocessing.cpu_count()
    rez = list(map(lambda x: x/cpu_count, get_load()))
    pango_print(rez)
