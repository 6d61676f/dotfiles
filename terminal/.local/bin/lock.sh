#!/usr/bin/env bash
#set -x

if [ -z "$(pidof xss-lock)" ]; then
    exit 1
else
    sleep 2
    xset dpms force off
fi
