#!/usr/bin/env bash

[ -x "$(command -v curl)" ] || exit 1

curl "wttr.in/~$1"
