#!/usr/bin/env bash

#set -x
set -euo pipefail

#function exit_msg() {
#    msg="Quitting low battery monitor" 
#    wall "$msg" 
#    notify-send "$msg"
#}
#
#trap exit_msg SIGINT
#trap exit_msg SIGQUIT

function warn() {
    msg="Battery is low"
    wall "$msg"
    notify-send "$msg"
}

function critical() {
    msg="Battery is critical!! Plug charger or prepare for shutdown."
    wall "$msg"
    notify-send -u critical "$msg"
}

warned=1
warn_thresh=30
crit_thresh=10
sleep_secs=60

while true; do    
        read -r status capacity <<< "$(acpi -b | awk -F'[:%,]' '{print $2, $3}')"
        if [ "$status" = Discharging ]; then
            if [ "$capacity" -lt $crit_thresh ]; then
                critical
            elif [ $warned -eq 1 ] && [ "$capacity" -lt $warn_thresh ]; then
                warn
                warned=0
            fi
        elif [ "$status" = Charging ]; then
            warned=1
        fi
    sleep "$sleep_secs"s
done


