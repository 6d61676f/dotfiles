#!/usr/bin/env bash
set -euo pipefail
set -x


autolock=$(command -v xss-lock)
lock=$(command -v slock)
notify=$(command -v notify-send)

xset s 300 30
$autolock -n "$notify -u normal -t 10000 -- 'LOCKING screen in 30 seconds'" -- "$lock"
