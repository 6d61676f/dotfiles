#!/usr/bin/env bash
#set -x

city=${1:-"Bucharest"}
output="$(curl --silent "wttr.in/~$city\?0\?T\?Q" 2>/dev/null)"

awk -v city="$city" '
    {
        if(NR==1) {
            printf "%s: %s", city, $NF
        } else if(NR==2) {
            printf " %s%s\n", $(NF-1), $NF
        }
    }
' <(echo "$output")
