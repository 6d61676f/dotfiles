set nocompatible               " be iMproved, required
set number                     " show line number
filetype off                   " required
filetype plugin on
syntax on
set t_Co=256                   " Terminal 256 colors
set encoding=utf-8
set shortmess+=I               " No intro message
set hidden                     " Allow buffer switching without saving
set smarttab
set expandtab                  " Tabs are spaces, not tabs
set shiftwidth=4               " Use indents of 4 spaces
set tabstop=4                  " An indentation every four columns
set softtabstop=4              " Let backspace delete indent
set cursorline                 " Highlight line with cursor
set backspace=indent,eol,start " Backspace for dummies
set linespace=0                " No extra spaces between rows
set showmatch                  " Show matching brackets/parenthesis
set incsearch                  " Find as you type search
set hlsearch                   " Highlight search terms
set ignorecase                 " Case insensitive search
set smartcase                  " Case sensitive when uc present
set wildmenu                   " Show list instead of just completing
"set wildmode=list:longest,full " Command <Tab> completion, list matches, then longest common part, then all.
set nojoinspaces               " Prevents inserting two spaces after punctuation on a join (J)
set splitright                 " Puts new vsplit windows to the right of the current
set splitbelow                 " Puts new split windows to the bottom of the current
set clipboard=unnamed          " Access your system clipboard with this line

"Setting up vim-plug
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
                \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

"Plugin 'vim-airline/vim-airline'
"Plugin 'vim-airline/vim-airline-themes'
Plug 'itchyny/lightline.vim'
Plug 'ap/vim-buftabline'

Plug 'mileszs/ack.vim'
Plug 'junegunn/fzf.vim'

Plug 'bfrg/vim-cpp-modern'

Plug 'w0rp/ale'
"Plugin 'scrooloose/syntastic' "Considering replacing with ale
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/nerdcommenter'
Plug 'severin-lemaignan/vim-minimap'

Plug 'tomasr/molokai'
Plug 'skielbasa/vim-material-monokai'
Plug 'junegunn/seoul256.vim'

""It seems that neoformat is faster
Plug 'sbdchd/neoformat'

Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

""Replace with vim-easy-align
"Plugin 'godlygeek/tabular'

Plug 'bronson/vim-trailing-whitespace'
Plug 'majutsushi/tagbar'
Plug 'Yggdroot/indentLine'
Plug 'mbbill/undotree'
Plug 'kshenoy/vim-signature'
Plug 'vim-scripts/DoxygenToolkit.vim' " Testing
Plug 'junegunn/vim-easy-align'        " Testing
"Plug 'artnez/vim-wipeout'             " Testing
"Plug 'dhruvasagar/vim-table-mode'     " Testing

"" Vim Snippets
"Plug 'SirVer/ultisnips'
"Plug 'honza/vim-snippets'

" Distraction-free dev
Plug 'junegunn/limelight.vim'
Plug 'junegunn/goyo.vim'

"" Python
"Plug 'nvie/vim-flake8'
"
""Rust
"Plug 'racer-rust/vim-racer'
"Plug 'rust-lang/rust.vim'

"" Haskell Bundle
"Plug 'eagletmt/neco-ghc'
"Plug 'Shougo/vimproc.vim', {'do' : 'make'}
"Plug 'eagletmt/ghcmod-vim'
""Plugin 'dag/vim2hs'
"Plug 'neovimhaskell/haskell-vim'
"Plug 'nbouscal/vim-stylish-haskell'

""Deoplete + Clang
"if has('nvim')
"  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"else
"  Plug 'Shougo/deoplete.nvim'
"  Plug 'roxma/nvim-yarp'
"  Plug 'roxma/vim-hug-neovim-rpc'
"endif
"Plug 'zchee/deoplete-clang'
"Plug 'Shougo/echodoc.vim'


" All of your Plugins must be added before the following line
call plug#end()
" Put your non-Plugin stuff after this line

"" Map leader to ,
let mapleader=','

"" Syntastic
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list            = 1
"let g:syntastic_check_on_open            = 1
"let g:syntastic_check_on_wq              = 0
"let g:syntastic_cpp_compiler             = "g++"
"let g:syntastic_cpp_compiler_options     = "-std=c++11 -Wall -Wextra"

"" Colorscheme
"let g:molokai_original = 1
"let g:rehash256 = 1
"color molokai
set background=dark
"set termguicolors "No color on tmux and some TERMS
colorscheme material-monokai

""StatusLine
"set laststatus=2
"set statusline  =
"set statusline +=%#PmenuSel#
"set statusline +=%{fugitive#statusline()}
"set statusline +=%#LineNr#
"set statusline +=\ %t
"set statusline +=%m
"set statusline +=%=
"set statusline +=%#CursorColumn#
"set statusline +=\ %{tagbar#currenttag('%s','')}
"set statusline +=\ %y
"set statusline +=\ %{&fileencoding?&fileencoding:&encoding}
"set statusline +=\[%{&fileformat}\]
"set statusline +=\ %p%%
"set statusline +=\ %l:%c

"Lightline
set noshowmode
set laststatus=2
function! ShowTag()
    return tagbar#currenttag('[%s]', '', 's')
endfunction
let g:lightline =  {
            \   'colorscheme':'solarized',
            \   'active': {
            \       'left': [ [ 'mode', 'paste' ],
            \                 [ 'gitbranch', 'readonly', 'modified' ],
            \                 [ 'current_tag' ] ] ,
            \       'right': [ [ 'percent' ] , [ 'lineinfo' ] ]
            \   },
            \ 'component_function': {
            \   'gitbranch': 'fugitive#head',
            \   'current_tag': 'ShowTag'
            \ },
            \ }

"" Airline
"set laststatus=2
"let g:airline#extensions#tabline#enabled = 1
"" https://github.com/powerline/fonts
"let g:airline_powerline_fonts = 1

"""ALE
"let g:ale_sign_error         = '>>'
"let g:ale_sign_warning       = '--'
"let g:ale_sign_column_always = 1
"let g:ale_lint_on_enter      = 1
""let g:ale_open_list          = 1
"let g:ale_fixers = {
"            \    'cpp': [
"            \        'clang-format',
"            \        'uncrustify',
"            \    ],
"            \    'c': [
"            \        'clang-format',
"            \        'uncrustify',
"            \    ],
"            \}
"
"" Rust related stuff
"let g:rustfmt_autosave             = 1
"let g:syntastic_rust_checkers      = ['cargo']
"let g:racer_experimental_completer = 1
"au FileType rust nmap gd <Plug>(rust-def)
"au FileType rust nmap gs <Plug>(rust-def-split)
"au FileType rust nmap gx <Plug>(rust-def-vertical)
"au FileType rust nmap <leader>gd <Plug>(rust-doc)
"
""Deoplete
"let g:deoplete#enable_at_startup = 1
"let g:deoplete#sources#clang#libclang_path = '/usr/lib/llvm-6.0/lib/libclang.so'
"let g:deoplete#sources#clang#clang_header = '/usr/lib/clang/6.0/include'

"" YouCompleteMe
"" https://raw.githubusercontent.com/JDevlieghere/dotfiles/master/.vim/.ycm_extra_conf.py
"let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
""let g:ycm_extra_conf_globlist = [ '~/Projects/*' ]
"""Do not autocomplete while writing
""let g:ycm_min_num_of_chars_for_completion = 99
""" Go to Declaration
""map <leader>df :YcmCompleter GoToDeclaration<CR>
""" Go to Definition
""map <leader>if :YcmCompleter GoToDefinition<CR>
""" Get type
""map <leader>sf :YcmCompleter GetType<CR>

"" vim-clang
"let g:clang_auto = 0

"" clang-complete
"let g:clang_complete_auto = 0
"let g:clang_close_preview = 1

"" Neco-ghc
"let g:haskellmode_completion_ghc = 0
"autocmd FileType haskell setlocal omnifunc=necoghc#omnifunc
"
""Haskell-Vim
"let g:haskell_enable_quantification   = 1 " to enable highlighting of `forall`
"let g:haskell_enable_recursivedo      = 1 " to enable highlighting of `mdo` and `rec`
"let g:haskell_enable_arrowsyntax      = 1 " to enable highlighting of `proc`
"let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
"let g:haskell_enable_typeroles        = 1 " to enable highlighting of type roles
"let g:haskell_enable_static_pointers  = 1 " to enable highlighting of `static`
"let g:haskell_backpack                = 1 " to enable highlighting of backpack keywords

"Goyo
let g:goyo_width = '90%'
let g:goyo_height = '90%'

"" Util-Snips
"let g:UltiSnipsExpandTrigger       = "<c-z>"
"let g:UltiSnipsListSnippets        = "<c-c>"
"let g:UltiSnipsJumpForwardTrigger  = "<c-j>"
"let g:UltiSnipsJumpBackwardTrigger = "<c-k>"

" Open Buffer
noremap <leader>bh :new<CR>
noremap <leader>bv :vnew<CR>

"" Buffer nav
noremap <leader>k :bn<CR>
noremap <leader>j :bp<CR>

"" Split
noremap <Leader>- :<C-u>split<CR>
noremap <Leader>\ :<C-u>vsplit<CR>

"" Close buffer
noremap <leader>d :bd<CR>

"" Switching windows
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
noremap <C-h> <C-w>h

" Wrapped lines goes down/up to next row, rather than next line in file.
noremap j gj
noremap k gk


" No More arrows
noremap <Up> <nop>
noremap <Down> <nop>
noremap <Left> <nop>
noremap <Right> <nop>

"" Binary editor in vim
nnoremap <leader>he :Hexmode<CR>
inoremap <leader>he <Esc>:Hexmode<CR>
vnoremap <leader>he :<C-U>Hexmode<CR>

" ex command for toggling hex mode - define mapping if desired
command -bar Hexmode call ToggleHex()

" helper function to toggle hex mode
function ToggleHex()
    " hex mode should be considered a read-only operation
    " save values for modified and read-only for restoration later,
    " and clear the read-only flag for now
    let l:modified=&mod
    let l:oldreadonly=&readonly
    let &readonly=0
    let l:oldmodifiable=&modifiable
    let &modifiable=1
    if !exists("b:editHex") || !b:editHex
        " save old options
        let b:oldft=&ft
        let b:oldbin=&bin
        " set new options
        setlocal binary " make sure it overrides any textwidth, etc.
        silent :e " this will reload the file without trickeries
        "(DOS line endings will be shown entirely )
        let &ft="xxd"
        " set status
        let b:editHex=1
        " switch to hex editor
        %!xxd
    else
        " restore old options
        let &ft=b:oldft
        if !b:oldbin
            setlocal nobinary
        endif
        " set status
        let b:editHex=0
        " return to normal editing
        %!xxd -r
    endif
    " restore values for modified and read only state
    let &mod=l:modified
    let &readonly=l:oldreadonly
    let &modifiable=l:oldmodifiable
endfunction

" Call Make
nnoremap <F9> :silent make -s \|redraw!\| cc<CR>

"Undo Tree
nnoremap <F5> :UndotreeToggle<cr>

" Tagbar
nmap <silent> <F4> :TagbarToggle<CR>
let g:tagbar_autofocus = 1

" NerdTree
map <C-n> :NERDTreeToggle<CR>

"Clang-format
let g:clang_format#auto_format = 0
let g:clang_format#code_style  = 'mozilla'

" Fzf
let fzfHelp=expand('~/.fzf/README.md')
if filereadable(fzfHelp)
    set rtp+=~/.fzf
else
    set rtp+=~/.antigen/bundles/junegunn/fzf
endif
map <Leader>b :Buffers<CR>
map <Leader>f :Files<CR>
map <Leader>t :Tags<CR>

" Ack.vim
if executable('rg')
    let g:ackprg = 'rg --vimgrep '
endif

if has ("cscope")
    " use both cscope and ctag for 'ctrl-]', ':ta', and 'vim -t'
    set cscopetag

    " check cscope for definition of a symbol before checking ctags: set to 1
    " if you want the reverse search order.
    set csto=1

    " add any cscope database in current directory
    if filereadable("cscope.out")
        cs add cscope.out
    endif

    function! LoadCscope()
        let db = findfile("cscope.out", ".;")
        if (!empty(db))
            let path = strpart(db, 0, match(db, "/cscope.out$"))
            set nocscopeverbose " suppress 'duplicate connection' error
            exe "cs add " . db . " " . path
            set cscopeverbose
        endif
    endfunction
    au BufEnter /* call LoadCscope()

    set tags=tags;/

    let cscp=expand('~/Documents/Tags/cscope.out')
    if filereadable(cscp)
        silent cs add ~/Documents/Tags/cscope.out
    endif

    let ctgs=expand('~/Documents/Tags/tags')
    if filereadable(ctgs)
        set tags=~/Documents/Tags/tags
    endif

    nmap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>g :cs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>c :cs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>t :cs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
    nmap <C-\>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-\>d :cs find d <C-R>=expand("<cword>")<CR><CR>


    " Using 'CTRL-spacebar' (intepreted as CTRL-@ by vim) then a search type
    " makes the vim window split horizontally, with search result displayed in
    " the new window.
    "
    " (Note: earlier versions of vim may not have the :scs command, but it
    " can be simulated roughly via:
    "    nmap <C-@>s <C-W><C-S> :cs find s <C-R>=expand("<cword>")<CR><CR>

    nmap <C-@>s :scs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>g :scs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>c :scs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>t :scs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>e :scs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>f :scs find f <C-R>=expand("<cfile>")<CR><CR>
    nmap <C-@>i :scs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-@>d :scs find d <C-R>=expand("<cword>")<CR><CR>


    " Hitting CTRL-space *twice* before the search type does a vertical
    " split instead of a horizontal one (vim 6 and up only)
    "
    " (Note: you may wish to put a 'set splitright' in your .vimrc
    " if you prefer the new window on the right instead of the left

    nmap <C-@><C-@>s :vert scs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>g :vert scs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>c :vert scs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>t :vert scs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>e :vert scs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>f :vert scs find f <C-R>=expand("<cfile>")<CR><CR>
    nmap <C-@><C-@>i :vert scs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-@><C-@>d :vert scs find d <C-R>=expand("<cword>")<CR><CR>


    "" show msg when any other cscope db added
    "set cscopeverbose

    """"""""""""" My cscope/vim key mappings
    "
    " The following maps all invoke one of the following cscope search types:
    "
    "   's'   symbol: find all references to the token under cursor
    "   'g'   global: find global definition(s) of the token under cursor
    "   'c'   calls:  find all calls to the function name under cursor
    "   't'   text:   find all instances of the text under cursor
    "   'e'   egrep:  egrep search for the word under cursor
    "   'f'   file:   open the filename under cursor
    "   'i'   includes: find files that include the filename under cursor
    "   'd'   called: find functions that function under cursor calls
    "
    " Using 'CTRL-spacebar' (intepreted as CTRL-@ by vim) then a search type
    " makes the vim window split horizontally, with search result displayed in
    " the new window.
    " Hitting CTRL-space *twice* before the search type does a vertical
    " split instead of a horizontal one (vim 6 and up only)
    "
endif


"Folds
"zi 	switch folding on or off
"zo 	open current fold
"zO 	recursively open current fold
"zc 	close current fold
"zC 	recursively close current fold
"za 	toggle current fold
"zA 	recursively open/close current fold
"zm 	reduce `foldlevel` by one
"zM 	close all folds
"zr 	increase `foldlevel` by one
"zR 	open all folds
"zj 	move down to top of next fold
"zk 	move up to bottom of previous fold

""Slows down vim very much
""Fold code by syntax
"set foldenable
"set foldmethod=syntax
"set foldlevelstart=2
"nnoremap <Space> za


"Marks
"  mx           Toggle mark 'x' and display it in the leftmost column
"  dmx          Remove mark 'x' where x is a-zA-Z
"
"  m,           Place the next available mark
"  m.           If no mark on line, place the next available mark. Otherwise, remove (first) existing mark.
"  m-           Delete all marks from the current line
"  m<Space>     Delete all marks from the current buffer
"  ]`           Jump to next mark
"  [`           Jump to prev mark
"  ]'           Jump to start of next line containing a mark
"  ['           Jump to start of prev line containing a mark
"  `]           Jump by alphabetical order to next mark
"  `[           Jump by alphabetical order to prev mark
"  ']           Jump by alphabetical order to start of next line having a mark
"  '[           Jump by alphabetical order to start of prev line having a mark
"  m/           Open location list and display marks from current buffer
"
"  m[0-9]       Toggle the corresponding marker !@#$%^&*()
"  m<S-[0-9]>   Remove all markers of the same type
"  ]-           Jump to next line having a marker of the same type
"  [-           Jump to prev line having a marker of the same type
"  ]=           Jump to next line having a marker of any type
"  [=           Jump to prev line having a marker of any type
"  m?           Open location list and display markers from current buffer
"  m<BS>        Remove all markers

