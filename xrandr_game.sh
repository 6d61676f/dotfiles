#!/bin/sh
xrandr --output DP-0 --mode 1920x1080 --pos 0x0 --rotate left --output DVI-D-0 --mode 1920x1080 --pos 1080x0 --rotate normal --output DP-1 --off --output HDMI-0 --primary --mode 1920x1080 --pos 3000x0 --rotate normal
pacmd set-card-profile 0 output:hdmi-stereo-extra2
pacmd set-default-sink alsa_output.pci-0000_01_00.1.hdmi-stereo-extra2
amixer set Master 20%
nitrogen --restore
