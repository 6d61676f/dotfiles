#!/usr/bin/env bash
set -euo pipefail
#set -x

killall -u "$(whoami)" polybar || :

monitors="$(xrandr --listmonitors)"
pri_mon=( $(awk '$0 ~ /*+/ {print $4}' <<<$monitors) )
sec_mon=( $(awk 'NR>1 {print $4}' <<<$monitors) )
sec_mon=( "${sec_mon[@]/${pri_mon[@]}}" )

#if [ ${#sec_mon[@]} -ne 0 ]; then
#    xrandr_external_layout.sh
#else
#    xrandr_internal_layout.sh
#fi

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

if [ -z "${pri_mon[*]}" ]; then
    MONITOR=${sec_mon[0]} polybar primary &

    for ((i=1;i<${#sec_mon[@]};i++))
    do
        if [[ -n "${sec_mon[i]}" ]]; then
            MONITOR=${sec_mon[i]} polybar secondary &
        fi
    done
else
    MONITOR=${pri_mon[0]} polybar primary &

    for ((i=0;i<${#sec_mon[@]};i++))
    do
        if [ -n "${sec_mon[i]}" ]; then
            MONITOR=${sec_mon[i]} polybar secondary &
        fi
    done
fi


##notify-send 'Fix multi-monitor' &
