#!/usr/bin/env sh

for i in $(pgrep -f 'polybar top'); do
    polybar-msg -p "$i" cmd toggle
done
