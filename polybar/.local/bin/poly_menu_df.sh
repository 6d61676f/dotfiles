#!/usr/bin/env bash

disks="$(df -hl -x tmpfs -x devtmpfs | awk 'NR>1 {print $6" "$4}')"
notify-send "Free Disk Space" "$disks"
