#!/usr/bin/env bash

city="${1:-Bucharest}"
#val="$(curl "wttr.in/~$city?0QAT" 2>/dev/null)"
val="$(curl "wttr.in/~$city?0QAT" 2>/dev/null)"
notify-send "Weather Report $city" "$val"
