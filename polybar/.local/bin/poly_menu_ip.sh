#!/usr/bin/env bash

ipz="$(ip a show scope global  | awk '/inet/ {print $NF" - "$2}')"
notify-send "IP Addresses" "$ipz"

