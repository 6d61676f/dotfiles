
#!/usr/bin/env bash

awk -v cpu_num=$(nproc --all 2>/dev/null || echo 1) -F '[/| ]' '
    function color(val, min, max) {
        green = "#00ff00"
        yellow = "#ffff00"
        red = "#ff0000"
        mid = (max+min) / 2
        three = 3/4*max
        if(val<mid) {
            return green
        } else if(val <= three) {
            return yellow
        } else {
            return red
        }
    }

    BEGIN {
        l1=0 #One Min Load
        l5=0 #Five Min Load
        l10=0 #Ten Min Load
        r_p=0 #running processes
        t_p=0 #total processes
        l_p=0 #last given PID
    }

    {
        l1 = $1
        l5 = $2
        l10 = $3
        r_p = $4
        t_p = $5
        l_p = $6
    }

    END {
        c1=color(l1, 0, cpu_num)
        c5=color(l5, 0, cpu_num)
        c10=color(l10, 0, cpu_num)
        printf "%{F%s}%s%{F-} ", c1, l1
        printf "%{F%s}%s%{F-} ", c5, l5
        printf "%{F%s}%s%{F-}", c10, l10
        #printf " %d %d %d\n", r_p, t_p, l_p
    }
' /proc/loadavg
