#!/bin/sh
xrandr --output DP-0 --mode 1920x1080 --pos 0x0 --rotate left --output DVI-D-0 --primary --mode 1920x1080 --pos 1080x0 --rotate normal --output DP-1 --off --output HDMI-0 --off
pacmd set-default-sink alsa_output.pci-0000_00_1b.0.analog-stereo
amixer set Master 0%
nitrogen --restore
