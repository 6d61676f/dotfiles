#!/usr/bin/env bash
set -euo pipefail

stow="$(command -v stow)"

if [ -z "$stow" ]; then
    echo 'Stow is not in path'
    exit 1
else
    dirs="$(find ./ -maxdepth 1 -mindepth 1 -type d -a -not \( -path '\./\.*' \))"
    for dir in $dirs; do
        subdirs=$(find "${dir}" -mindepth 1 -type d)
        for subd in $subdirs; do
            mkdir -p "${HOME}/${subd#$dir/}"
        done
        echo -n "beSTOW dotfiles upon $dir? [yn] "
        read -r resp
        if [[ "$resp" =~ "y" ]]; then
            "$stow" "${dir##*/}" -t "$HOME"
            echo "Stowed $dir"
        fi
    done
fi
exit 0
