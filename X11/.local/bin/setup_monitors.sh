#!/usr/bin/env bash
set -euo pipefail

monitors=( $(xrandr -q | awk '/ connected/ {print $1}') )

if [ ${#monitors[@]} -gt 1 ]; then
    xrandr_external_layout.sh
else
    xrandr_internal_layout.sh
fi

nitrogen --restore
#~/.config/polybar/launch.sh
